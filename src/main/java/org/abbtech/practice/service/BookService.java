package org.abbtech.practice.service;

import org.abbtech.practice.model.Book;

import java.util.List;
import java.util.UUID;

public interface BookService {
    Book addBook(Book book);

    List<Book> getAllBooks();

    Book getBookById(UUID id);
}
