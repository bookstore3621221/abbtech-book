package org.abbtech.practice.service.imp;

import lombok.RequiredArgsConstructor;
import org.abbtech.practice.model.Book;
import org.abbtech.practice.repository.BookRepository;
import org.abbtech.practice.service.BookService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {
    private final BookRepository bookRepository;

    @Override
    public Book addBook(Book book) {
        return bookRepository.save(book);
    }

    @Override
    public List<Book> getAllBooks() {
        return bookRepository.findAll();
    }

    @Override
        public Book getBookById(UUID id) {
        return bookRepository.findById(id).orElse(null);
    }
}
