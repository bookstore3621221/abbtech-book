package org.abbtech.practice.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.abbtech.practice.model.Book;
import org.abbtech.practice.service.BookService;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/books")
@RequiredArgsConstructor
@Tag(name = "Book Controller", description = "API for managing books")
public class BookController {

    private final BookService bookService;

    @PostMapping
    @Operation(summary = "Add a new book")
    public Book addBook(@RequestHeader(value = "X-USER-ID", required = false) String userId,
                        @RequestHeader(value = "X-USER-EMAIL", required = false) String userEmail, @RequestBody
                        Book book) {
        return bookService.addBook(book);
    }

    @GetMapping
    @Operation(summary = "Get all books")
    public List<Book> getAllBooks(@RequestHeader(value = "X-USER-ID", required = false) String userId,
                                  @RequestHeader(value = "X-USER-EMAIL", required = false) String userEmail) {
        return bookService.getAllBooks();
    }

    @GetMapping("/{id}")
    @Operation(summary = "Get book by ID")
    public Book getBookById(@RequestHeader(value = "X-USER-ID", required = false) String userId,
                            @RequestHeader(value = "X-USER-EMAIL", required = false) String userEmail,
                            @PathVariable UUID id) {
        return bookService.getBookById(id);
    }
}
